From 4558dc5590b89b1252baea2734c2b3668566e5cb Mon Sep 17 00:00:00 2001
From: Peng Liang <liangpeng10@huawei.com>
Date: Mon, 7 Sep 2020 14:07:07 +0800
Subject: [PATCH] target/arm: ignore evtstrm and cpuid CPU features

evtstrm and cpuid cann't be controlled by VMM:
1. evtstrm: The generic timer is configured to generate events at a
   frequency of approximately 100KHz.  It's controlled by the linux
   kernel config CONFIG_ARM_ARCH_TIMER_EVTSTREAM.
2. cpuid: EL0 access to certain ID registers is available.  It's always
   set by linux kernel after 77c97b4ee2129 ("arm64: cpufeature: Expose
   CPUID registers by emulation").
However, they are exposed by getauxval() and /proc/cpuinfo.  Hence,
let's report and ignore the CPU features if someone set them.

Signed-off-by: Peng Liang <liangpeng10@huawei.com>
Signed-off-by: Dongxu Sun <sundongxu3@huawei.com>
---
 target/arm/cpu64.c | 29 ++++++++++++++++++++++++++++-
 1 file changed, 28 insertions(+), 1 deletion(-)

diff --git a/target/arm/cpu64.c b/target/arm/cpu64.c
index 9e5179afbe..287e7ac91c 100644
--- a/target/arm/cpu64.c
+++ b/target/arm/cpu64.c
@@ -982,10 +982,37 @@ static gchar *aarch64_gdb_arch_name(CPUState *cs)
     return g_strdup("aarch64");
 }
 
+static const char *unconfigurable_feats[] = {
+    "evtstrm",
+    "cpuid",
+    NULL
+};
+
+static bool is_configurable_feat(const char *name)
+{
+    int i;
+
+    for (i = 0; unconfigurable_feats[i]; ++i) {
+        if (g_strcmp0(unconfigurable_feats[i], name) == 0) {
+            return false;
+        }
+    }
+
+    return true;
+}
+
 static void
 cpu_add_feat_as_prop(const char *typename, const char *name, const char *val)
 {
-    GlobalProperty *prop = g_new0(typeof(*prop), 1);
+    GlobalProperty *prop;
+
+    if (!is_configurable_feat(name)) {
+        info_report("CPU feature '%s' is not configurable by QEMU.  Ignore it.",
+                    name);
+        return;
+    }
+
+    prop = g_new0(typeof(*prop), 1);
     prop->driver = typename;
     prop->property = g_strdup(name);
     prop->value = g_strdup(val);
-- 
2.27.0

