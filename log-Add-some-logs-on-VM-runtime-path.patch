From d0ed3afacd2af1cbfcfb615471ade3c8c4185c00 Mon Sep 17 00:00:00 2001
From: Yan Wang <wangyan122@huawei.com>
Date: Tue, 8 Feb 2022 15:48:01 +0800
Subject: [PATCH] log: Add some logs on VM runtime path

Add logs on VM runtime path, to make it easier to do trouble shooting.

Signed-off-by: Ying Fang <fangying1@huawei.com>
Signed-off-by: Yan Wang <wangyan122@huawei.com>
---
 hw/virtio/virtio-pci.c |  2 ++
 hw/virtio/virtio.c     | 14 ++++++++++++--
 monitor/monitor.c      |  9 +++++++++
 qapi/qmp-dispatch.c    | 15 +++++++++++++++
 softmmu/qdev-monitor.c |  4 +++-
 5 files changed, 41 insertions(+), 3 deletions(-)

diff --git a/hw/virtio/virtio-pci.c b/hw/virtio/virtio-pci.c
index 750aa47ec1..38a5dc1ba8 100644
--- a/hw/virtio/virtio-pci.c
+++ b/hw/virtio/virtio-pci.c
@@ -1772,7 +1772,9 @@ static void virtio_pci_device_unplugged(DeviceState *d)
     VirtIOPCIProxy *proxy = VIRTIO_PCI(d);
     bool modern = virtio_pci_modern(proxy);
     bool modern_pio = proxy->flags & VIRTIO_PCI_FLAG_MODERN_PIO_NOTIFY;
+    VirtIODevice *vdev = virtio_bus_get_device(&proxy->bus);
 
+    qemu_log("unplug device name: %s\n", !vdev ? "NULL" : vdev->name);
     virtio_pci_stop_ioeventfd(proxy);
 
     if (modern) {
diff --git a/hw/virtio/virtio.c b/hw/virtio/virtio.c
index ea7c079fb0..9b4ac58a16 100644
--- a/hw/virtio/virtio.c
+++ b/hw/virtio/virtio.c
@@ -1945,7 +1945,14 @@ int virtio_set_status(VirtIODevice *vdev, uint8_t val)
         k->set_status(vdev, val);
     }
     vdev->status = val;
-
+    if (val) {
+        qemu_log("%s device status is %d that means %s\n",
+                 vdev->name, val,
+                 (val & VIRTIO_CONFIG_S_DRIVER_OK) ? "DRIVER OK" :
+                 (val & VIRTIO_CONFIG_S_DRIVER) ? "DRIVER" :
+                 (val & VIRTIO_CONFIG_S_ACKNOWLEDGE) ? "ACKNOWLEDGE" :
+                 (val & VIRTIO_CONFIG_S_FAILED) ? "FAILED" : "UNKNOWN");
+    }
     return 0;
 }
 
@@ -2389,8 +2396,11 @@ VirtQueue *virtio_add_queue(VirtIODevice *vdev, int queue_size,
             break;
     }
 
-    if (i == VIRTIO_QUEUE_MAX || queue_size > VIRTQUEUE_MAX_SIZE)
+    if (i == VIRTIO_QUEUE_MAX || queue_size > VIRTQUEUE_MAX_SIZE) {
+        qemu_log("unacceptable queue_size (%d) or num (%d)\n",
+                 queue_size, i);
         abort();
+    }
 
     vdev->vq[i].vring.num = queue_size;
     vdev->vq[i].vring.num_default = queue_size;
diff --git a/monitor/monitor.c b/monitor/monitor.c
index 21c7a68758..013c628695 100644
--- a/monitor/monitor.c
+++ b/monitor/monitor.c
@@ -29,6 +29,7 @@
 #include "qapi/qapi-emit-events.h"
 #include "qapi/qapi-visit-control.h"
 #include "qapi/qmp/qdict.h"
+#include "qapi/qmp/qjson.h"
 #include "qemu/error-report.h"
 #include "qemu/option.h"
 #include "sysemu/qtest.h"
@@ -318,6 +319,7 @@ static void monitor_qapi_event_emit(QAPIEvent event, QDict *qdict)
 {
     Monitor *mon;
     MonitorQMP *qmp_mon;
+    GString *json;
 
     trace_monitor_protocol_event_emit(event, qdict);
     QTAILQ_FOREACH(mon, &mon_list, entry) {
@@ -328,6 +330,13 @@ static void monitor_qapi_event_emit(QAPIEvent event, QDict *qdict)
         qmp_mon = container_of(mon, MonitorQMP, common);
         if (qmp_mon->commands != &qmp_cap_negotiation_commands) {
             qmp_send_response(qmp_mon, qdict);
+            json = qobject_to_json(QOBJECT(qdict));
+            if (json) {
+                if (!strstr(json->str, "RTC_CHANGE")) {
+                    qemu_log("%s\n", json->str);
+                }
+                g_string_free(json, true);
+            }
         }
     }
 }
diff --git a/qapi/qmp-dispatch.c b/qapi/qmp-dispatch.c
index d378bccac7..bb005594d3 100644
--- a/qapi/qmp-dispatch.c
+++ b/qapi/qmp-dispatch.c
@@ -25,6 +25,7 @@
 #include "qapi/qmp/qbool.h"
 #include "qemu/coroutine.h"
 #include "qemu/main-loop.h"
+#include "qemu/log.h"
 
 Visitor *qobject_input_visitor_new_qmp(QObject *obj)
 {
@@ -147,6 +148,7 @@ QDict *qmp_dispatch(const QmpCommandList *cmds, QObject *request,
     QObject *id;
     QObject *ret = NULL;
     QDict *rsp = NULL;
+    GString *json;
 
     dict = qobject_to(QDict, request);
     if (!dict) {
@@ -204,6 +206,19 @@ QDict *qmp_dispatch(const QmpCommandList *cmds, QObject *request,
         qobject_ref(args);
     }
 
+    json = qobject_to_json(QOBJECT(args));
+    if (json) {
+        if ((strcmp(command, "query-block-jobs") != 0)
+            && (strcmp(command, "query-migrate") != 0)
+            && (strcmp(command, "query-blockstats") != 0)
+            && (strcmp(command, "query-balloon") != 0)
+            && (strcmp(command, "set_password") != 0)) {
+                qemu_log("qmp_cmd_name: %s, arguments: %s\n",
+                         command, json->str);
+        }
+        g_string_free(json, true);
+    }
+
     assert(!(oob && qemu_in_coroutine()));
     assert(monitor_cur() == NULL);
     if (!!(cmd->options & QCO_COROUTINE) == qemu_in_coroutine()) {
diff --git a/softmmu/qdev-monitor.c b/softmmu/qdev-monitor.c
index 01f3834db5..dfd6429bf3 100644
--- a/softmmu/qdev-monitor.c
+++ b/softmmu/qdev-monitor.c
@@ -36,6 +36,7 @@
 #include "qemu/option.h"
 #include "qemu/qemu-print.h"
 #include "qemu/option_int.h"
+#include "qemu/log.h"
 #include "sysemu/block-backend.h"
 #include "migration/misc.h"
 #include "migration/migration.h"
@@ -635,6 +636,7 @@ DeviceState *qdev_device_add_from_qdict(const QDict *opts,
     if (path != NULL) {
         bus = qbus_find(path, errp);
         if (!bus) {
+            error_setg(errp, "can not find bus for %s", driver);
             return NULL;
         }
         if (!object_dynamic_cast(OBJECT(bus), dc->bus_type)) {
@@ -707,7 +709,7 @@ DeviceState *qdev_device_add_from_qdict(const QDict *opts,
     if (*errp) {
         goto err_del_dev;
     }
-
+    qemu_log("add qdev %s:%s success\n", driver, dev->id ? dev->id : "none");
     if (!qdev_realize(DEVICE(dev), bus, errp)) {
         goto err_del_dev;
     }
-- 
2.27.0

